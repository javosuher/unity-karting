﻿using UnityEngine;

namespace KartGame.KartSystems
{
    /// <summary>
    /// A basic keyboard implementation of the IInput interface for all the input information a kart needs.
    /// </summary>
    public class AndroidInput : MonoBehaviour, IInput
    {
        #region Inherit Methods
        
        public float Acceleration
        {
            get { return acceleration; }
        }

        public float Steering
        {
            get { return steering; }
        }

        public bool BoostPressed
        {
            get { return boostPressed; }
        }

        public bool FirePressed
        {
            get { return firePressed; }
        }

        public bool HopPressed
        {
            get { return hopPressed; }
        }

        public bool HopHeld
        {
            get { return hopHeld; }
        }
        
        #endregion

        // Variables used to set the input
        float acceleration;
        float steering;
        bool hopPressed;
        bool hopHeld;
        bool boostPressed;
        bool firePressed;
        bool fixedUpdateHappened;

        /// <summary>
        /// Method executed when the game starts
        /// </summary>
        private void Start()
        {
            // The car always run towards
            acceleration = 1f;
            
            // Unused input in Android devices
            boostPressed = false;
            firePressed = false;
        }

        /// <summary>
        /// This method is executed each frame
        /// </summary>
        void Update()
        {
            if (fixedUpdateHappened)
            {
                fixedUpdateHappened = false;
                hopHeld = false;
                hopPressed = false;
            }

            // If only a finger is touching the screen
            if (Input.touchCount == 1)
            {
                foreach (Touch touch in Input.touches)
                {
                    if (touch.phase == TouchPhase.Began)
                    {
                        if (touch.position.x < Screen.width / 2) steering = -1f; // left touch
                        else if (touch.position.x > Screen.width / 2) steering = 1f; // Right touch
                    }
                }
            }
            // If the user touch using two fingers the car hop
            else if (Input.touchCount >= 2)
            {
                hopHeld = true;
                hopPressed = true;
            }
            // If the user not touch the screen
            else
            {
                steering = 0f;
            }
        }

        /// <summary>
        /// This method is executed each frame in physics time simulated
        /// </summary>
        void FixedUpdate()
        {
            fixedUpdateHappened = true;
        }
    }
}