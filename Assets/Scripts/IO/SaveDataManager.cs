﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace KartGame.IO
{
    /// <summary>
    /// This class is used to save and load persistent data in the game.
    /// Each saved file should be a serializable class.
    /// </summary>
    public static class SaveDataManager
    {
        #region Public constants

        /// <summary>
        /// The default path of the save data file
        /// </summary>
        public const string DEFAULT_PATH = "SaveData.cf";

        /// <summary>
        /// The path slash used to create the file path
        /// </summary>
        public const string PATH_SLASH = "/";

        #endregion

        #region Protected variables

        /// <summary>
        /// Auxiliary variable used to serialize and deserialize the classes
        /// </summary>
        private static BinaryFormatter binaryFormatter = new BinaryFormatter();

        /// <summary>
        /// Cache to store the saved files loaded and saved.
        /// </summary>
        private static Dictionary<string, object> savedDataCache = new Dictionary<string, object>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Save a serialized class in a file.
        /// Warning: If there is a file with the same path, the file will be override
        /// </summary>
        /// <param name="serializableClass">The class to serialize</param>
        /// <param name="path">The path to save the file</param>
        /// <typeparam name="T">The serialized class</typeparam>
        public static void Save<T>(T serializableClass, string path = DEFAULT_PATH)
        {
            // Check if is a serializable class 
            if (!IsASerializableClass<T>()) return;

            // Write the data to save
            string filePath = Application.persistentDataPath + PATH_SLASH + path;
            FileStream fileStream = new FileStream(filePath, FileMode.Create);
            binaryFormatter.Serialize(fileStream, serializableClass);
            fileStream.Close();

            // Add this file to the cache
            savedDataCache[filePath] = serializableClass;
        }

        /// <summary>
        /// Load a serialized class stored in a file.
        /// </summary>
        /// <param name="path">The path of the file stored</param>
        /// <typeparam name="T">The serialized class</typeparam>
        /// <returns>The serialized class with the stored data</returns>
        public static T Load<T>(string path = DEFAULT_PATH)
        {
            // Check if is a serializable class 
            if (!IsASerializableClass<T>()) return default;

            string filePath = Application.persistentDataPath + PATH_SLASH + path;
            if (savedDataCache.ContainsKey(filePath)) // If is in cache
            {
                // The serialized class must be the same stored
                if (!IsTheSameClassStored<T>(savedDataCache[filePath])) return default;
                return (T) savedDataCache[filePath];
            }
            else // If not in cache we need to read the file
            {
                // Check if the file is saved in the cache
                if (savedDataCache.ContainsKey(filePath)) return (T) savedDataCache[filePath];

                if (File.Exists(filePath))
                {
                    // Read the file
                    FileStream fileStream = new FileStream(filePath, FileMode.Open);
                    object serializableClass = binaryFormatter.Deserialize(fileStream);
                    fileStream.Close();

                    // The serialized class must be the same stored
                    if (!IsTheSameClassStored<T>(serializableClass)) return default;
                    savedDataCache[filePath] = serializableClass;
                    return (T) serializableClass;
                }
                else
                {
                    Debug.LogError("[SaveDataManager] Save data file not found: " + filePath);
                    return default;
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Check if a class is serializable
        /// </summary>
        /// <typeparam name="T">The type of the class to check</typeparam>
        /// <returns>True if is serializable</returns>
        private static bool IsASerializableClass<T>()
        {
            Type classType = typeof(T);
            if (classType.IsSerializable) return true;
            Debug.LogError("[SaveDataManager] The class " + classType.Name + " must be serializable");
            return false;
        }

        /// <summary>
        /// Check if the save data class type is the same of the desired one
        /// </summary>
        /// <param name="classStored">The save data file content</param>
        /// <typeparam name="T">The class type to cast</typeparam>
        /// <returns>True if the data stored is the same type</returns>
        private static bool IsTheSameClassStored<T>(object classStored)
        {
            if (classStored.GetType() == typeof(T)) return true;
            Debug.LogError("[SaveDataManager] The class " + typeof(T).Name +
                           " is not the same class previously saved in the file");
            return false;
        }

        #endregion
    }
}