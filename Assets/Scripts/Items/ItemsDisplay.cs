﻿using System;
using System.Text;
using System.Collections.Generic;
using KartGame.Track;
using TMPro;
using UnityEngine;

namespace KartGame.Items
{
    /// <summary>
    /// This class update the text to show the user the collectable items achieved in the current game
    /// </summary>
    public class ItemsDisplay : MonoBehaviour
    {
        #region Enums

        /// <summary>
        /// The different item collector information can be displayed on screen
        /// </summary>
        public enum DisplayOptions
        {
            /// <summary>
            /// The number of coins collected in the current race
            /// </summary>
            coins
        }

        #endregion
        
        #region Public Parameters 
        
        /// <summary>
        /// The collectable items values displayed and the order to display them
        /// </summary>
        [Tooltip("The collectable items values displayed and the order to display them")]
        public DisplayOptions[] initialDisplayOptions;
        
        /// <summary>
        /// A reference to the track manager to get important data
        /// </summary>
        [Tooltip("A reference to the track manager to get important data")]
        public TrackManager trackManager;
        
        /// <summary>
        /// A reference to the TextMeshProUGUI to display the information
        /// </summary>
        [Tooltip("A reference to the TextMeshProUGUI to display the information")]
        public TextMeshProUGUI textComponent;
        
        #endregion
        
        #region Protected Parameters 
        
        /// <summary>
        /// The list of display values to show to the user
        /// </summary>
        List<Action> displayCalls = new List<Action>();

        /// <summary>
        /// The message which contains all information
        /// </summary>
        protected StringBuilder messageToShow = new StringBuilder(0, 300);

        #endregion
        
        #region Protected Methods

        /// <summary>
        /// Method executed when the game starts
        /// </summary>
        protected void Awake()
        {
            // Configure all display options
            foreach (DisplayOptions option in initialDisplayOptions)
            {
                switch (option)
                {
                    case DisplayOptions.coins:
                        displayCalls.Add(DisplayCoinsNumber);
                        break;
                }
            }
        }

        /// <summary>
        /// Method executed each frame of the game
        /// </summary>
        protected void Update()
        {
            messageToShow.Clear();

            // Create the text data
            foreach (Action action in displayCalls)
            {
                action.Invoke();
            }

            textComponent.text = messageToShow.ToString();
        }

        /// <summary>
        /// This method create the text of the number of coins collected by the player
        /// </summary>
        protected void DisplayCoinsNumber()
        {
            long numberCoins = trackManager.GetNumberCoinsCollected(); 
            if(numberCoins > 0) messageToShow.AppendLine("Coins: " + numberCoins);
        }
        
        #endregion
    }
}