﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace KartGame.Items
{
    /// <summary>
    /// Class used to add functionalities to a collectable items.
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class CollectableItem : MonoBehaviour
    {
        #region Public variables

        /// <summary>
        /// Disable this game object when other collider start colliding with our one
        /// </summary>
        [Tooltip("Disable this game object when other collider start colliding with our one")]
        public bool DisableOnTriggerEnter = true;
        
        /// <summary>
        /// Disable this game object when other collider finish colliding with our one
        /// </summary>
        [Tooltip("Disable this game object when other collider finish colliding with our one")]
        public bool DisableOnTriggerExit = false;

        #endregion
        
        #region Unity Events

        [Header("Item Collision Events")]
        /// <summary>
        /// Unity event invoked when other collider start colliding with our one
        /// </summary>
        [Tooltip("Unity events invoked when other collider start colliding with our one")]
        public UnityEvent ItemOnTriggerEnter;

        /// <summary>
        /// Unity event invoked when other collider finish colliding with our one
        /// </summary>
        [Tooltip("Unity events invoked when other collider finish colliding with our one")]
        public UnityEvent ItemOnTriggerExit;

        /// <summary>
        /// Unity event invoked when other collider is currently colliding with our one
        /// </summary>
        [Tooltip("Unity events invoked when other collider is currently colliding with our one")]
        public UnityEvent ItemOnTriggerStay;

        #endregion

        #region C# Events

        /// <summary>
        /// Event invoked when other collider start colliding with our one
        /// </summary>
        public event Action<GameObject, Collider> onTriggerEnter;

        /// <summary>
        /// Event invoked when other collider finish colliding with our one
        /// </summary>
        public event Action<GameObject, Collider> onTriggerExit;

        /// <summary>
        /// Event invoked when other collider is currently colliding with our one
        /// </summary>
        public event Action<GameObject, Collider> onTriggerStay;

        #endregion

        #region protected Methods

        /// <summary>
        /// Method executed when other collider start colliding with our one
        /// </summary>
        /// <param name="other">The other collider</param>
        protected virtual void OnTriggerEnter(Collider other)
        {
            ItemOnTriggerEnter?.Invoke();
            onTriggerEnter?.Invoke(this.gameObject, other);
            if(DisableOnTriggerEnter) gameObject.SetActive(false);
        }

        /// <summary>
        /// Method executed when other collider finish colliding with our one
        /// </summary>
        /// <param name="other"></param>
        protected virtual void OnTriggerExit(Collider other)
        {
            ItemOnTriggerExit?.Invoke();
            onTriggerExit?.Invoke(this.gameObject, other);
            if(DisableOnTriggerExit) gameObject.SetActive(false);
        }

        /// <summary>
        /// Method executed when other collider is currently colliding with our one
        /// </summary>
        /// <param name="other"></param>
        protected virtual void OnTriggerStay(Collider other)
        {
            ItemOnTriggerStay?.Invoke();
            onTriggerStay?.Invoke(this.gameObject, other);
        }

        #endregion
    }
}