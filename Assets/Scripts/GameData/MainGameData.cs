﻿using System;

namespace KartGame.GameData
{
    /// <summary>
    /// This class has the main global data of the game.
    /// This data is saved in a file.
    /// </summary>
    [Serializable]
    public class MainGameData
    {
        /// <summary>
        /// The best lap time achieved in the game
        /// </summary>
        public float bestLapTime = float.MaxValue;

        /// <summary>
        /// The maximum of coins collected by the player
        /// </summary>
        public long maxCoins = 0;
    }
}