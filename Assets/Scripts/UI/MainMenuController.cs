﻿using System.Text;
using KartGame.GameData;
using KartGame.IO;
using TMPro;
using UnityEngine;

namespace KartGame.UI
{
    /// <summary>
    /// This class has all functionalities of the main menu of the game
    /// </summary>
    public class MainMenuController : MonoBehaviour
    {
        #region Public variables

        /// <summary>
        /// The text which should contains the global main data
        /// </summary>
        [Tooltip("The text which should contains the global main data")]
        public TextMeshProUGUI gameDataText;

        #endregion

        #region Protected variables

        /// <summary>
        /// The message which contains all information
        /// </summary>
        protected StringBuilder messageToShow = new StringBuilder(0, 300);

        #endregion

        #region Protected Methods

        /// <summary>
        /// Method executed whn the game starts
        /// </summary>
        protected void Start()
        {
            // Load global game data
            MainGameData mainGameData = SaveDataManager.Load<MainGameData>();
            if (mainGameData != null)
            {
                // Get the best time lap to show it to the user
                if (mainGameData.bestLapTime < float.MaxValue)
                    CreateBestTimeLapText(mainGameData);
                // Get total coins collected
                if (mainGameData.maxCoins > 0)
                    CreateMaximumCoinsCollectedText(mainGameData);
            }

            gameDataText.text = messageToShow.ToString();
        }

        protected void CreateBestTimeLapText(MainGameData mainGameData)
        {
            messageToShow.AppendLine("Best Lap Time: " + mainGameData.bestLapTime + " seconds");
        }

        protected void CreateMaximumCoinsCollectedText(MainGameData mainGameData)
        {
            messageToShow.AppendLine("Total Coins Collected: " + mainGameData.maxCoins);
        }

        #endregion
    }
}