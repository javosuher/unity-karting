﻿using TMPro;
using UnityEngine;

namespace KartGame.UI
{
    /// <summary>
    /// This class has all functionalities of the record canvas of the game
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class RecordController : MonoBehaviour
    {
        #region Public variables

        /// <summary>
        /// The animator controller to show the record panel
        /// </summary>
        [Tooltip("The animator controller to show the record panel")]
        public Animator animatorController;

        /// <summary>
        /// The text which should contains the record text
        /// </summary>
        [Tooltip("The text which should contains the record text")]
        public TextMeshProUGUI gameDataText;

        #endregion

        #region Protected variables

        /// <summary>
        /// Contant to set the trigger in the animator controller to show the record panel
        /// </summary>
        protected const string triggerAnimation = "Show";

        #endregion

        #region Public Methods

        /// <summary>
        /// This method show a desired record in the game to the user
        /// </summary>
        /// <param name="text">The text record to show</param>
        public void ShowRecord(string text)
        {
            gameDataText.text = text;
            animatorController.SetTrigger(triggerAnimation);
        }

        #endregion

        #region Protected Methods

        /// <summary>
        /// Method executed when the game starts
        /// </summary>
        protected void Start()
        {
            // Set default values
            Reset();
        }

        /// <summary>
        /// Method executed when the component is reset or when is placed in a game object
        /// </summary>
        protected void Reset()
        {
            // Assign the game required game object components
            if (animatorController == null)
                animatorController = GetComponent<Animator>();
        }

        #endregion

        #region Debug Methods

        [ContextMenu("ShowRecord")]
        void Debug00()
        {
            ShowRecord("Example Record");
        }

        #endregion
    }
}