﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace KartGame.Utils
{
    /// <summary>
    /// This class manages the scene load, it can be used as well to preload the scene or to add scene to the game
    /// </summary>
    public class SceneChanger : MonoBehaviour
    {
        /// <summary>
        /// This function unloads asynchronously a scene passed by parameter
        /// </summary>
        /// <param name="sceneName">The scene to be unloaded</param>
        public void UnloadSceneAsync(string sceneName)
        {
            SceneManager.UnloadSceneAsync(sceneName);
        }

        /// <summary>
        /// This function loads a scene in a single way
        /// </summary>
        /// <param name="sceneName">The name of the scene to be loaded in a single way</param>
        public void LoadSceneSingle(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        /// <summary>
        /// This function loads a scene additively
        /// </summary>
        /// <param name="sceneName">The name of the scene to be loaded additively</param>
        public void LoadSceneAdditive(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        }
    }
}