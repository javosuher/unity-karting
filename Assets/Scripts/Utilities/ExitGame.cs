﻿using UnityEngine;

namespace KartGame.Utils
{
    /// <summary>
    /// This class is used to exit the game
    /// </summary>
    public class ExitGame : MonoBehaviour
    {
        /// <summary>
        /// Exits the current game
        /// </summary>
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}